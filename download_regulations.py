#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  download_regulations.py
#
"""
    Project Dependencies:
        --- pip install paramiko
"""
import os
import paramiko

PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))


def get_from_server(host_addr,
                    username="ubuntu",
                    pem_file="experiment.pem",
                    msg=None):
    msg = dict(Status='UNKNOWN', Error=[]) if not msg else msg
    try:
        pem_key = paramiko.RSAKey.from_private_key_file(os.path.join(PROJECT_PATH, pem_file))
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname=host_addr, username=username, pkey=pem_key)
        sftp_client = ssh_client.open_sftp()
        sftp_client.chdir('federal_regulations/')
        data_dir = sftp_client.listdir('data/')
        for reg_file in data_dir:
            if os.path.exists(os.path.join(os.path.join(PROJECT_PATH, 'records/'),
                                           os.path.basename(reg_file))):
                continue

            sftp_client.get(os.path.join('data/', reg_file),
                            os.path.join(os.path.join(PROJECT_PATH, 'records/'),
                                         os.path.basename(reg_file)))
        ssh_client.close()
    except Exception as e:
        msg['Status'] = 'FAILED'
        msg['Error'].append(repr(e))


def get_files():
    remote_servers = list()
    with open(os.path.join(PROJECT_PATH, 'server_info.txt'), 'r') as servers_file:
        for server in servers_file.readlines():
            if server.strip() == '':
                continue
            remote_servers.append(server.strip())

    for host_address in remote_servers:
        msg = dict(Status='UNKNOWN', Error=[])
        get_from_server(host_address, msg=msg)
        if msg['Status'] != 'FAILED':
            print "Successfully, downloaded files from %s" % host_address
        else:
            print "Failed, to download files from %s" % host_address
            print "Reason: %s" % msg['Error']


if __name__ == '__main__':
    """
        This python script will visit all remote servers and will try do download all files from it
        moreover it will merge all of them at one space.
    """
    if not os.path.exists(os.path.join(PROJECT_PATH, 'records/')):
        os.makedirs(os.path.join(PROJECT_PATH, 'records/'), 0755)
    get_files()
