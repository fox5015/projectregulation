#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  fetch_regulations.py
#
"""
    Project Dependencies:
        --- pip install requests
        --- pip install paramiko
        --- pip install simplejson
"""

import os
import datetime
import paramiko


base_url = 'www.federalregister.gov/api/v1/articles.json?'
base_url += 'per_page=1000&order=relevance&conditions%5Bpublication_date%5D%5Bis%5D='


PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))


def do_server_work(host_addr,
                   commands=[],
                   upload_files=[],
                   username="ubuntu",
                   pem_file="experimentNew.pem",
                   msg=None):
    msg = dict(Status='UNKNOWN', Error=[]) if not msg else msg
    try:
        pem_key = paramiko.RSAKey.from_private_key_file(os.path.join(PROJECT_PATH, pem_file))
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname=host_addr, username=username, pkey=pem_key)
        if bool(upload_files):
            sftp_client = ssh_client.open_sftp()
            remote_path = 'federal_regulations/'
            try:
                sftp_client.chdir(remote_path)
            except IOError:
                sftp_client.mkdir(remote_path)
                sftp_client.chdir(remote_path)
            for upload_file in upload_files:
                sftp_client.put(upload_file, os.path.basename(upload_file))
        stdin, stdout, stderr = ssh_client.exec_command('cd federal_regulations/')
        for command in commands:
            stdin, stdout, stderr = ssh_client.exec_command(command)
            if stderr:
                msg['Error'].append(stderr.read())
        ssh_client.close()
    except Exception as e:
        msg['Status'] = 'FAILED'
        msg['Error'].append(repr(e))


def distribute_work(msg=None):
    msg = dict(Status='UNKNOWN', Error=[]) if not msg else msg
    try:

        document_urls = list()
        base_start = datetime.date(1994, 1, 1)
        base_end = datetime.datetime.now().date()
        for x in range((base_end - base_start).days):
            dt = base_start + datetime.timedelta(days=x)
            document_urls.append(base_url + dt.strftime('%Y-%m-%d'))

        print "Hey, we are going to downloads %d files!! " % (len(document_urls) * 1000)

        remote_servers = list()
        with open(os.path.join(PROJECT_PATH, 'server_info.txt'), 'r') as servers_file:
            for server in servers_file.readlines():
                if server.strip() == '':
                    continue
                remote_servers.append(server.strip())

        server_count = len(remote_servers)
        document_load_count = len(document_urls) / server_count

        print "Hey, per node %d files!! " % document_load_count

        start_count = 0
        end_count = document_load_count
        for host_address in remote_servers:
            if not host_address:
                continue
            msg = dict(Status='UNKNOWN', Error=[])
            document_share = document_urls[start_count:end_count]

            with open(os.path.join(PROJECT_PATH, 'urls.txt'), 'w') as url_file:
                url_file.write('\n'.join(document_share))

            do_server_work(host_address, upload_files=[os.path.join(PROJECT_PATH, 'urls.txt'),
                                                       os.path.join(PROJECT_PATH, 'server.py'),
                                                       os.path.join(PROJECT_PATH, 'existing_documents.txt')], msg=msg)
            if msg["Status"] != "FAILED":
                print "Successfully, done first part with %s" % host_address
                os.remove(os.path.join(PROJECT_PATH, 'urls.txt'))

            #  After work
            start_count += document_load_count
            end_count += end_count

        print "Done, writing urls of documents that needed to be download!!"
        done_status = open('executed.bin', 'wb')
        done_status.close()

    except KeyError:
        msg["Status"] = "FAILED"
        msg["Error"].append("Try Again! executing this script, or try again with new API KEY")

    except Exception as e:
        msg["Status"] = "FAILED"
        msg["Error"].append("Oops, something seriously went wrong contact the developer! %s " % repr(e))


if __name__ == '__main__':
    """
        This python script will be responsible to divide the workload of downloading
        document b/w multiple remote server nodes
    """
    msg = dict(Status='UNKNOWN', Error=[])
    if not os.path.exists(os.path.join(PROJECT_PATH, 'executed.bin')):
        distribute_work(msg)


