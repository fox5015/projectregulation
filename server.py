#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  server.py
#
"""
    Project Dependencies:
        --- pip install requests
"""
import os
import requests


PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))


def download_file(document_id, download_url):
    try:
        response = requests.get(download_url, stream=True)

        if not response:
            return

        if 199 < response.status_code < 400:
            local_filename = os.path.join(PROJECT_PATH, "data/%s.pdf" % document_id)
            with open(local_filename, 'wb') as f:
                for chunk in response.iter_content(chunk_size=1024):
                    if chunk:
                        f.write(chunk)
        else:
            print "Document: ", download_url

    except requests.exceptions.Timeout:
        print "Document: ", download_url

    except requests.exceptions.RequestException as e:
        print "Document: ", download_url

    except Exception as e:
        print "Document: ", download_url


def make_records(urls_file="urls.txt", existing_documents='existing_documents.txt'):

    def download_url(url):
        regulation_docs = {}
        try:
            response = requests.get('https://' + url, timeout=10)
            if not response:
                return regulation_docs
            if 199 < response.status_code < 400:
                js_response = response.json()
                results = js_response.get('results', [])
                for document in results:
                    if not document:
                        continue
                    if document.get('pdf_url', None) and document.get('document_number', None):
                        regulation_docs[document['document_number']] = document['pdf_url']
        except Exception as e:
            print "Page:", url
        return regulation_docs

    try:
        document_urls = list()
        with open(os.path.join(PROJECT_PATH, urls_file), 'r') as url_file:
            for url in url_file.readlines():
                document_urls.append(url.strip())

        exisiting_doc_ids = list()
        with open(os.path.join(PROJECT_PATH, existing_documents), 'r') as doc_file:
            for doc_id in doc_file.readlines():
                exisiting_doc_ids.append(doc_id.strip())

        for doc_url in document_urls:

            documents = download_url(doc_url)
            for doc_id, doc_html in documents.items():
                if doc_id in exisiting_doc_ids:
                    continue
                download_file(doc_id, doc_html)


    except Exception as e:
        print "Oops, something seriously went wrong contact the developer! %s " % repr(e)


if __name__ == '__main__':
    """
        This python script will work on remote server which will be responsible
        for downloading document ids from document urls and then downloading
        documents
    """
    if not os.path.exists(os.path.join(PROJECT_PATH, 'data/')):
        os.makedirs(os.path.join(PROJECT_PATH, 'data/'))
    make_records()
